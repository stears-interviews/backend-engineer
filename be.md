# Stears Coding Homework

Thanks for your application to Stears

This is a coding homework test for Stears’ Senior Backend Engineer role. We expect that you will use this test to demonstrate your abilities. Your solution does not need to be perfect, but keep in mind we are hiring for someone to improve and maintain our APIs to serve content to both our mobile and web apps.

For this role, we prefer software engineer candidates with:
- Experience building backend with Python, preferably Django
- Experience deploying containers, preferably with docker

For each task, you will be given a back story and a couple of tasks to summarise what needs to be done with a strict definition of done. Either complete all of them or attempt as much as possible so we have enough information to assess your skill & experience.

#### Submissions

- Create a private [gitlab](https://gitlab.com/) repository and add @foluso_ogunlana & @ssani as maintainers
- Create a README.md with the following:
  - Clear set up instructions (assume no knowledge of the submission or the stack)
  - Notes of any new useful tools / patterns you discovered while completing the test

#### Deadlines

You will be given at least 5 working days to complete the task and your deadline will be communicated via email.

#### Assessment

Our favourite submissions are -

1. **Clean & simple code** - Minimal, high quality and well tested code that gets to the heart of the problem and stops there.
2. **Rigorous** - Complete solutions with well researched applications of the right technology choices.
3. **UX & DevX** - Smooth user / developer experience which is easy to deploy, well documented and easy to contribute to.

Your submission will also impress us by demonstrating one or more of the following -

- **Mastery** in your general use of language, libraries, frameworks, architecture / design / deployment patterns
- **Unique skills** in specific areas - particularly data engineering, devops & testing

#### Conduct

By submitting, you assure us that you have not shared the test with anyone else and that all work submitted is your own. Though you are allowed to use whatever online or offline resources you see fit to learn skills for the tasks.

# **Coding Homework**

**Guidelines**
- Complete all tasks if possible
- Include a README.md telling us how to use your code
- BE - Use Python, preferably Django.
- Database - PostgreSQL

## Background

You have been contracted by Tell-All, an online blog, to redesign their blog in an attempt to move away from their current Wordpress site. The new blog must allow an administrator to write a blog post, edit the post and publish it. Visitors will be able to read or favorite posts and to donate money to the blog.

As a Senior Backend Engineer in the team, you are tasked with building the backend of the proposed redesign.

### Task 1

You need to create an API that allows an administrator to add, read, update and delete posts on the blog; and to allow anyone to search, filter and paginate posts on the endpoint `/blog/posts`. Each post must have a title, slug, summary, cover picture, author, HTML body, and tags for searching.

You're going to hand over the API to the frontend developers before you go on holiday. So you'll need to create comprehensive documentation that they can use to tell what the API can do. 

Hints:
- We don't require that you write all the code yourself. There are lots of packages that can do the work for you

**Definition of done**:

- [ ] An administrator must be able to see all existing posts and modify or create a new one somehow
- [ ] Comprehensive documentation provided for all API endpoints
- [ ] The post must have 2 possible states - Draft, Published
- [ ] The post must have 2 possible types - Freemium and Premium
- [ ] An anonymous user must be able to filter for posts by tag, slug, or type
- [ ] The author of the post must be the administrator that created the post
- [ ] An anonymous user must be able to fetch all posts which are published using `GET /blog/posts` (defaults to a limit of 5 posts)
- [ ] `GET /blog/posts?offset=5&limit=5` must fetch the 6th-10th posts
- [ ] Unpublished posts should never be fetched by `GET /blog/posts`
- [ ] All posts must have the following attributes
    - Title
    - Slug
    - Summary
    - Cover picture
    - Author email address
    - Tags
    - Status
    - Type
- [ ] It should be possible to run your API server in a single command
- [ ] It should be possible to build and run your API along with any dependencies using "docker compose up" in a single command
- [ ] It should be possible to run automated tests against your API endpoints using a single command

### Task 2

Your API is a hit, but there are so many posts being created and read on your blog, that the authors are finding it difficult to keep track. First, there is a strict publishing cycle in the blog. Premium posts are published at 5am, and Freemium posts are published at 3pm.

This means the editing administrator must be available at these exact times every day to publish the posts. In addition to that, the administrator who authored the post needs to know when the post has been published so they can go online and promote it.

Can you solve these problems for the author and editor?
- Allow an admin add a publish date to a post, and automatically publish it on the scheduled date
- Notify editors when a post that they authored has been scheduled

**Definition of done**:

- [ ] Admins can set a custom "pub_date" in the future for each post
- [ ] An admin cannot set pub_date <= today
- [ ] At 5am on the "pub_date" of a Premium post, it's status is updated from "Draft" to "Published"
- [ ] At 3pm on the "pub_date" of a Freemium post, it's status is updated from "Draft" to "Published"
- [ ] Once a post is published, the author is notified of this via email
- [ ] It should be possible to run your API server in a single command
- [ ] It should be possible to build and run your API along with any dependencies using "docker compose up" in a single command
- [ ] It should be possible to run automated tests against this functionality using a single command

## Thanks!

If you made it this far, thanks for your time.
We look forward to reviewing your solid application with you!
